package io.scenarium.audio;

import io.scenarium.audio.operator.speaker.Beep;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(Beep.class);
	}

}
