import io.scenarium.audio.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.audio {
	requires transitive io.scenarium.pluginmanager;

	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.logger;
	requires transitive io.scenarium.core;
	requires transitive io.scenarium.flow;

	exports io.scenarium.audio;
	exports io.scenarium.audio.operator.speaker;

}
